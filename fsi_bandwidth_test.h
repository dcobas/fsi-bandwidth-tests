
#ifndef __FSIBANDWIDTH_LIB_H__
#define __FSIBANDWIDTH_LIB_H__


#define TX_BUFFER_SIZE_SAMPLES (32768*4)  

struct packet_header{
    
    int thread_id;
    int packet_id;
    int payload_size;
    int expected_pkt_n;
    int chan;
    int board;
    int first_sample_id;
    int sample_number;
};


struct packet_fsi{
    struct packet_header header;
    uint16_t payload[TX_BUFFER_SIZE_SAMPLES];
};

#endif
