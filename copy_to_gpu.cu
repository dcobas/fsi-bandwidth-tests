

#include <stdio.h>
#include <stdlib.h>
#include <cuda.h>
#include <string.h>
#include <time.h>
#include <cuda_runtime.h>
#include "fsi_bandwidth_test.h"


extern "C" int copy_to_gpu(struct packet_fsi *pkt, int** gpu_addr_return)
{
	int i=0;
	int N = pkt->header.sample_number;
        unsigned int nbytes = i*sizeof(int);
	int *cpu_payload = (int *)malloc(N*nbytes);
	for (i=0; i<N; i++)
	{
		cpu_payload[i]=(int)(pkt->payload[i]);
	}
        cudaMalloc(gpu_addr_return, N*nbytes);
	cudaMemcpy(*gpu_addr_return, cpu_payload, N, cudaMemcpyHostToDevice);	
	
	return 0;

}
