// Client side implementation of UDP client-server model
#define _GNU_SOURCE /* See feature_test_macros(7) */

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <sys/uio.h>
#include <netinet/in.h>
#include <sys/time.h>
#include <stdint.h>
#include <pthread.h>
#include "diot_fsi_regs.h"
#include <sched.h>
#include <sys/mman.h>
#include <fcntl.h>
#include <errno.h>
#include <math.h>
#include <time.h>
#include <netdb.h>

#include "copy_to_gpu.h"

uint64_t get_tics()
{
    struct timezone tz = {0, 0};
    struct timeval tv;

    gettimeofday(&tv, &tz);
    return tv.tv_sec * 1000000ULL + tv.tv_usec;
}

#define PORT 8080

#define MB_factor 1048576.0

#define C_UDP 0
#define C_TCP 1

#define S_SERVER 0
#define S_CLIENT 1
#define IP_ADDR_LEN 16 /* in theory max 15*/

/*
 * Data to be transmitted:
 * ~25ms of 12-bit data sample acquired at 100 MHz, then converted to uint16_t
 * 1 sample every 10ns
 * 
 * NOTE: here M=10e6 (rather than 2^20)
 * 
 * Per channel:
 *   Number of samples in 25ms :  2 500 000
 *   Number of bits of raw data: 30 000 000 = 12*2500000 = 3.75 MBytes
 *   Number of bits of uint16_t: 40 000 000 = 16*2500000 = 4.00 MBytes
 * 
 * Per DIOT chassis with 8*8=64 channels of packed data
 *   Number of samples in 25ms :   160 000 000 = 64*2500000
 *   Number of bits of raw data: 1 920 000 000 = 64*30000000 = 240 MBytes
 *   Number of bits of uint16_t: 2 560 000 000 = 64*40000000 = 320 MBytes
 * 
 */

#define TX_BUFFER_SIZE_SAMPLES (32768*4)
#define TX_BUFFER_SIZE_BYTES (2*TX_BUFFER_SIZE_SAMPLES)

#define RX_BUFFER_SIZE_BYTES 1048576


#define PACKED __attribute__((packed))

typedef struct {
    int c_type; //connection type (UPD/TCP)
    int s_type; //service type: client/server
    int s_payload;
    int n_packets;
    int n_threads;
    int n_cpus;
    int verbose;
    int fake_data; //data_mem;
    char ip_add[IP_ADDR_LEN];
    int n_board;  
    int n_chan;
    int n_sample;
    int n_pkt_chan;
    int rx_to_file;
    int rx_to_fbb;
    int acq_num;  // number of acquisitions to be send
    int check_dummy;
    int tx_read_mode; //0: unpack samples, 1: send raw data
} RunTimeOpts;

/*
 PACKED struct packet_header{
    // data used to check transmission
    int thread_id;
    int packet_id;
    int payload_size;
    int expected_pkt_n;
    // possible FSI header
    int chan;
    int board;
    int first_sample_id;
    int sample_number;
};


PACKED struct packet_fsi{
    struct packet_header header;
    uint16_t payload[TX_BUFFER_SIZE_SAMPLES];
};
*/


uint16_t fake_data_buffer[TX_BUFFER_SIZE_SAMPLES];
#define N_BOARD       8
#define N_CHAN        8
#define N_MAX_SAMPLES 2500000
#define FBB_SIZE_16BW (N_BOARD*N_CHAN*N_MAX_SAMPLES)
#define FBB_SIZE_32BW (FBB_SIZE_16BW/2)

// uint16_t fbb[N_CHAN*N_BOARD][N_MAX_SAMPLES];

uint16_t *fbb_16bw;
uint16_t *fbb_32bw;

RunTimeOpts rtOpts;

int psize = 7500;
int attempts = 100000;
int n_threads = 4;
int n_cpus = 2;

struct thread_state
{
    int id;
    uint64_t bytes_transferred;
    uint64_t pkts_transfered;
    uint64_t bytes_expected;
    uint64_t pkts_expected;
    double duration;
    uint64_t t_start;
    uint64_t t_end;
    pthread_t thread;
};

char* ip_address_def = "10.0.0.2";


int gpu_addr=0;

extern int copy_to_gpu(struct packet_fsi *pkt, int* gpu_addr_return);

// ----------------------------------- from diot_fsi.c/h ----------------------
#define DEBUG 0

#define M_AXI_BASE 0xb0000000
#define M_AXI_SIZE 0x10000

#define DMA0_BUF_ADDR 0x00000000 //0x1000 0000
#define DMA1_BUF_ADDR 0x10000000
#define PHYSICAL_ADDR 0x10000000 //0x80000000
//                       0xb0000000
// 
//                        800000000-87fffffff
//                        80000000-8000ffff
#define OFFSET 0


static void *m_axi_base;
static unsigned page_size;
int fsi_initialized=0;
static void *ddr_mem;

static void m_axi_writel(uint32_t addr, uint32_t data)
{
	if(addr > page_size)
        {
              printf("%s: address=%d goes beyond page boundry \n",__func__, addr);
              return;
        }
        *(volatile uint32_t *)(m_axi_base + addr) = data;
        if(rtOpts.verbose==3)printf("%s: Write: addr=0x%x | data=0x%x \n",__func__, addr, data);
}

static uint32_t m_axi_readl(uint32_t addr)
{
	uint32_t val;
	if(addr > page_size)
        {
              printf("%s: address=%d goes beyond page boundry \n",__func__, addr);
              return 0;
        }
        val = *(volatile uint32_t *)(m_axi_base + addr);
        if(rtOpts.verbose==3) printf("%s: Read: addr=0x%x | data=0x%x \n",__func__, addr, val);
        return val;
}

int m_axi_map(int addr)
{
        int fd;
        int value = 0;


        unsigned page_addr, page_offset;
        page_size=sysconf(_SC_PAGESIZE);

        /* Open /dev/mem file */
        fd = open ("/dev/mem",  O_RDWR );
        if (fd < 1) {
                printf("%s: /dev/mem: %s\n", __func__, strerror(errno));
                exit(1);
        }
        if(rtOpts.verbose==3) printf("%s: opened /dev/mem \n",__func__);
        /* mmap the device into memory */
        page_addr = (addr & (~(page_size-1)));
        page_offset = addr - page_addr;
        m_axi_base = mmap(NULL, page_size, PROT_READ|PROT_WRITE, MAP_SHARED, fd, page_addr);

        close(fd);
        if (m_axi_base == MAP_FAILED) {
                fprintf(stderr, "%s: mmap(/dev/mem): %s\n",__func__, strerror(errno));
                exit(1);
        }
        if(rtOpts.verbose==3) printf("%s: mmap() doen \n",__func__);

        /* Read value from the device register */

        value = (int)m_axi_readl(page_offset);
        printf("%s: Address = 0x%08x | Value = 0x%08x \n",__func__, page_addr, value);

//         munmap(m_axi_base, page_size);

        return value;
}

int m_axi_unmap(){

        unsigned page_size=sysconf(_SC_PAGESIZE);
        munmap(m_axi_base, page_size);
        return 0;
}



static unsigned
get_dma_length(void)
{
  return m_axi_readl(SYS_TOP_SUB_REG_LENGTH);
}
static void
dma_init(unsigned nsamp)
{
  m_axi_writel(SYS_TOP_SUB_REG_CONFIGS |
               SYS_TOP_SUB_REG_CONFIGS_ADDRESS, PHYSICAL_ADDR+DMA0_BUF_ADDR);
  m_axi_writel(SYS_TOP_SUB_REG_CONFIGS |
               SYS_TOP_SUB_REG_CONFIGS_ADDRESS |
               SYS_TOP_SUB_REG_CONFIGS_SIZE, PHYSICAL_ADDR+DMA1_BUF_ADDR);

  m_axi_writel(SYS_TOP_SUB_REG_LENGTH, nsamp);
}

void * mmap_ram(unsigned addr) {

        
    off_t offset = addr;
    size_t len = offset;

    
    size_t pagesize = sysconf(_SC_PAGE_SIZE);
    off_t page_base = (offset / pagesize) * pagesize;
    off_t page_offset = offset - page_base;

    int fd = open("/dev/mem", O_RDWR);
    printf("%s: open() OK, 0x%x,\n", __func__, (unsigned int)page_base);
//     void *mem = mmap(NULL, page_offset + len, PROT_READ | PROT_WRITE, MAP_PRIVATE, fd, page_base);
    void *mem = mmap(NULL, 0x20000000, PROT_READ | PROT_WRITE, MAP_SHARED, fd, page_base);
    if (mem == MAP_FAILED) {
        perror("Can't map memory");
    }
    close(fd);
    printf("%s: mmap OK\n", __func__);
    return mem;    

}


uint32_t raw_data(unsigned board, unsigned channel, unsigned off, void *mem)
{
    unsigned *samp =0;
    unsigned b;
    unsigned ch;

    /* Check bounds.  */
    if (board < 1 || board > 8)
      return 0;
    if (channel > 7)
      return 0;

    if (board <= 4)
      samp = (unsigned *)(mem + DMA0_BUF_ADDR);
    else
      samp = (unsigned *)(mem + DMA1_BUF_ADDR);

//     printf("%s: samp to memory\n", __func__ );
  /* Each sample is 1.5 bytes, so 48 (= 4*8*1.5) bytes per bank for all
     channels (8 chan per board, 4 boards per bank).
     48 bytes is 12 words.  */
    samp += 12 * off;

    b = 3 - ((board - 1) & 3);

//   /* Extract the data for 1 board (8 ch * 12 bits = 96 bits).  */
//     uint64_t hi =  samp[2 * b] | (((uint64_t)samp[2 * b + 1]) << 32);
//     uint32_t lo = samp[8 + b];
//   /* Extract the sample for the channel.  */
//     ch = channel;
//     uint32_t v = (((hi >> (8 * ch)) & 0xff) << 4) | ((lo >> (4 * ch)) & 0x0f);

  /* Extract the data for 1 board (8 ch * 12 bits = 96 bits).  */
  unsigned long hi =
    samp[2 * b] | (((unsigned long)samp[2 * b + 1]) << 32);
  unsigned lo = samp[8 + b];

  /* Extract the sample for the channel.  */
  ch = channel;
  unsigned v = (((hi >> (8 * ch)) & 0xff) << 4) | ((lo >> (4 * ch)) & 0x0f);



  return (uint32_t)v;
}
int cmd_fsi_regs(void)
{
  unsigned i;

  if(!m_axi_base)
     m_axi_map(M_AXI_BASE);

  printf("ident:   %08x\n",
              m_axi_readl(SYS_TOP_SUB_REG_IDENT));
  printf("status:  %08x\n",
              m_axi_readl(SYS_TOP_SUB_REG_STATUS));
  printf("control: %08x\n",
              m_axi_readl(SYS_TOP_SUB_REG_CONTROL));
  printf("config:  %08x\n",
              m_axi_readl(SYS_TOP_SUB_REG_CONFIG));
  printf("length:  %08x\n",
              m_axi_readl(SYS_TOP_SUB_REG_LENGTH));
  for (i = 0; i < 2; i++)
    {
      unsigned addr = SYS_TOP_SUB_REG_CONFIGS
        + (i * SYS_TOP_SUB_REG_CONFIGS_SIZE);
      printf("address  %u: %08x\n", i,
                 m_axi_readl(addr + SYS_TOP_SUB_REG_CONFIGS_ADDRESS));
      printf("cur addr %u: %08x\n", i,
                 m_axi_readl(addr + SYS_TOP_SUB_REG_CONFIGS_CUR_ADDRESS));
      printf("status   %u: %08x\n", i,
                 m_axi_readl(addr + SYS_TOP_SUB_REG_CONFIGS_STATUS));
    }
  printf("stat1:  %08x\n",
              m_axi_readl(SYS_TOP_SUB_REG_PERIPH_STAT1));
  printf("stat2:  %08x\n",
              m_axi_readl(SYS_TOP_SUB_REG_PERIPH_STAT2));
//   printf("cmd boards:  %08x\n",
//               m_axi_readl(SYS_TOP_SUB_REG_CMD_BOARDS));
//   printf("cmd value:   %08x\n",
//               m_axi_readl(SYS_TOP_SUB_REG_CMD_VALUE));
//   printf("cmd status:  %08x\n",
//               m_axi_readl(SYS_TOP_SUB_REG_CMD_STATUS));
//   for (i = 0; i < 8; i++)
//     printf("cmd reply[%u]: %08x\n", i,
//                 m_axi_readl(SYS_TOP_SUB_REG_CMD_REPLY | (i * 4)));
  return 0;
}

// ---------------- end of copy frm diot_fsi.-----------------------------------

uint16_t previous_data = 0;

int check_dummy_data(uint16_t data, int is_first)
{
  if(rtOpts.check_dummy == 0) 
    return 0; // check only if enabled
  
  if(is_first)
    previous_data = data;
  else
  {
    if(data == 0 && previous_data == 4095)
      previous_data = 0;
    else
    if(data == previous_data+1)
      previous_data = data;
    else
    {
      printf("Dummy data not correct - should be %d and is %d \n",previous_data+1, data);
      return -1;
    }
  }
  if(rtOpts.verbose==4) printf("Dummy data correct - should be %d and is %d \n",previous_data, data);
  return 0;
}

int new_dma_ready(void)
{
  uint32_t ret = m_axi_readl(SYS_TOP_SUB_REG_STATUS);
//   fprintf(stderr, "%d - ",ret);
  if ((ret & SYS_TOP_SUB_REG_STATUS_TRANSFER)  == 0 &&
      (ret & SYS_TOP_SUB_REG_STATUS_DMA_READY) != 0)
    return 1;
  else 
    return 0;
}
void clear_dma_ready(void)
{
  m_axi_writel(SYS_TOP_SUB_REG_STATUS, SYS_TOP_SUB_REG_STATUS_DMA_READY);
//   uint32_t ret = m_axi_readl(SYS_TOP_SUB_REG_STATUS);
//   printf("%d",ret);
  
}
void
fsi_trig_enable(unsigned en)
{
  unsigned val;

  val = m_axi_readl(SYS_TOP_SUB_REG_CONFIG);
  if (en)
  {
    val |= SYS_TOP_SUB_REG_CONFIG_TRIG_EN;
   fprintf(stderr,"[Enable trigger]");
  }
  else
  {
    val &= ~SYS_TOP_SUB_REG_CONFIG_TRIG_EN;
    fprintf(stderr,"[Disable trigger]");
  }
  m_axi_writel(SYS_TOP_SUB_REG_CONFIG, val);
}


FILE *create_file(int file_id,struct packet_fsi *pkt)
{
    FILE *fptr;
    char filename[30];
    time_t t = time(NULL);
    struct tm tm = *localtime(&t);
    sprintf(filename, "fsi_acq_%4d-%2d-%2d-%2dh%2dm%2ds_%d.dat", 
    tm.tm_year+1900, tm.tm_mon+1, tm.tm_mday,tm.tm_hour, tm.tm_min, tm.tm_sec,file_id);
    printf("[%s] Save to file: %s\n",__func__, filename);
    fptr=fopen(filename,"w");
    fprintf(fptr,"Acquisition for:\n");
    fprintf(fptr,"Board   number: %d\n", pkt->header.board);
    fprintf(fptr,"Channel number: %d (per board)\n", pkt->header.chan);
    fprintf(fptr,"Sample  number: %d (per channel)\n", pkt->header.sample_number);
    fprintf(fptr,"----------------------------------|\n");
    fprintf(fptr,"Board  | Channel| Sample | Value  |\n");
    return fptr;
}

FILE *create_file_per_chan(int board, int chan, int sample_number, char *base_name)
{
    FILE *fptr;
    char filename[50];
    time_t t = time(NULL);
    struct tm tm = *localtime(&t);
    sprintf(filename, "%s-b%d-ch%d.dat",base_name, board, chan);
    if(rtOpts.verbose==3) printf("[%s] Save to file: %s\n",__func__, filename);
    fptr=fopen(filename,"w");
    fprintf(fptr,"Acquisition for:\n");
    fprintf(fptr,"Board   number: %d\n", board);
    fprintf(fptr,"Channel number: %d (per board)\n", chan);
    fprintf(fptr,"Sample  number: %d (per channel)\n", sample_number);
    fprintf(fptr,"----------------------------------\n");
    return fptr;
}

int write_to_file(FILE *fptr, struct packet_fsi *pkt)
{
    int i;
    if(rtOpts.verbose==3) printf("Writing pkt id %d, board %d, cha %d, start sampe %d, num samples %d\n ",
      pkt->header.packet_id,
      pkt->header.board, 
      pkt->header.chan, 
      pkt->header.first_sample_id,
      pkt->header.sample_number
    );
    for(i=0;i< pkt->header.sample_number; i++)
    {
        fprintf(fptr,"%6d | %6d | %6d | %6d |\n",pkt->header.board,
                                                 pkt->header.chan,
                                                 pkt->header.first_sample_id+i,
                                                 pkt->payload[i]);
    }

}
int write_to_file_only_values(FILE *fptr, struct packet_fsi *pkt)
{
    int i, ret=0;
    if(rtOpts.verbose==3) printf("Writing pkt id %d, board %d, cha %d, start sampe %d, num samples %d\n ",
      pkt->header.packet_id,
      pkt->header.board, 
      pkt->header.chan, 
      pkt->header.first_sample_id,
      pkt->header.sample_number
    );
    for(i=0;i< pkt->header.sample_number; i++)
    {
        fprintf(fptr,"%d\n",pkt->payload[i]);
        ret = check_dummy_data(pkt->payload[i], pkt->header.first_sample_id == 0);
        if(ret<0)
          printf("Writing pkt id %d, board %d, cha %d, sample %d\n ",
              pkt->header.packet_id,
              pkt->header.board, 
              pkt->header.chan, 
              pkt->header.first_sample_id+i
            );
    }
}

int write_to_file_only_values_zeros(FILE *fptr, int pkt_id, int board, int chan, int s_per_ch)
{
    int i;
    if(rtOpts.verbose==3) printf("Writing pkt id %d, board %d, cha %d, num samples %d\n ",
      pkt_id,board, chan, s_per_ch);
    for(i=0;i< s_per_ch; i++)
    {
        fprintf(fptr,"%d\n",0);
    }
}


int write_to_file_entire_chan(FILE *fptr, uint16_t *buf, int board, int chan, int n_samples)
{
    int i, ret=0;
    if(rtOpts.verbose==3) printf("Writing entire chan %d of board %d to a file, num samples %d\n ",
      chan, board,n_samples );
    
    for(i=0;i<n_samples; i++)
    {
        fprintf(fptr,"%d\n",buf[i]);
        ret = check_dummy_data(buf[i], i == 0);
        if(ret<0)
          printf("Writing board %d, cha %d, sample %d\n ",
              board, chan, i);
    }
}

void info(void)
{
         printf(
         "\nUsage:  fsi_bandwidth_test_b [OPTION]\n\n"
         "\n"
         "-?                show this page\n"
         "-s                server mode [default] -> wait for data to be received\n"
         "-c                client mode -> transmit data\n"
         "-u                UDP transmission [default]\n"
         "-t                TCP tarnsmission \n"
         "-p payload_bytes  number of bytes in the FSI payload [60000 bytes by default] - client only\n"
         "-n packets_number number of packets sent by each thread [2000 packets by default] - client only\n"
         "-z thread_number  number of threads which send/receive the data [2 threads by default]\n"
         "-x CPU_number     number of CPUs used for transmission/reception of data [2 CPUs by default]\n"
         "-a total_bytes    number of bytes to be transfered in total (calculates number of frames needed\n"
         "-b board_number   transfer data from board_number (starting with 1), -1 for all boards\n"
         "-e bit_mask       transfer data from channels with bits set to 1, LSB=chan 0, , etc...\n"
         "-d sample_number  transfer particular number samples\n"
         "-g                safe t file\n"
         "-f                fake data\n"
         "-i IP address     destination IP address\n"
         "-j mode           Data to FBB (1:16bit samples, 2: 32bit raw data\n"
         "-k                Check dummy data (if ADC is set to send saw patter, check whether the data[i+1]=data[i]+1\n"
         "-h acq_number     Number of acquisitions to be tx-ed (if 0, a 1 acq, no wiat for DMA, otherwise wait DMA done)\n"
         "-l mode           Mode of reading data from RAM (0:unpack data, send per channel | 1: raw data, all channels in bank)\n"
         "-v level          level of verbosity [1 by default]\n"
         "\n");
}

int  startup(int argc, char **argv, RunTimeOpts *_rtOpts)
{
   int c,i;
   int num_cores = sysconf(_SC_NPROCESSORS_ONLN);
   int total_bytes = 0;
   int n_chan_per_board = 0;
   int n_boards_per_acq = 0;
   /// default values
   _rtOpts->c_type      = C_UDP ;
   _rtOpts->s_type      = S_SERVER;
   _rtOpts->s_payload   = 1400;//60000;
   _rtOpts->n_packets   = 2000;
   _rtOpts->n_threads   = 1;
   _rtOpts->n_cpus      = 1;
   _rtOpts->verbose     = 1;
   _rtOpts->fake_data   = 0;
   _rtOpts->n_board     = 7;
   _rtOpts->n_chan      = 0x7; //first 3 channesl
   _rtOpts->n_sample    = 2500000;
   _rtOpts->n_pkt_chan  = 0;
   _rtOpts->rx_to_file  = 0;
   _rtOpts->acq_num     = 0;
   _rtOpts->rx_to_fbb   = 0;
   _rtOpts->check_dummy = 0;
   _rtOpts->tx_read_mode= 0;
   strncpy(_rtOpts->ip_add,ip_address_def, sizeof(ip_address_def));
   
   if(argc == 1) 
   {
     info();
     return -1;
   }
   while( (c = getopt(argc, argv, "scutp:n:z:x:v:i:a:fb:e:d:gh:j:kl:")) != -1 ) {
     switch(c) {
     case 'i':
       printf("optarg: %s \n",optarg);
       strncpy(_rtOpts->ip_add, optarg, IP_ADDR_LEN);
       break;
     case 's':
       _rtOpts->s_type = S_SERVER;
       break;
     case 'c':
       _rtOpts->s_type = S_CLIENT;
       break;
     case 'u':
       _rtOpts->c_type = C_UDP ;
       break;
     case 't':
       _rtOpts->c_type = C_TCP ;
       break;
     case 'p':
       _rtOpts->s_payload = strtol(optarg, &optarg, 0);
       if (_rtOpts->s_payload >=TX_BUFFER_SIZE_BYTES)
       {
          printf("Packet size is too large - requested %d bytes, allowed %d bytes\n",
                 _rtOpts->s_payload, TX_BUFFER_SIZE_BYTES);
          return -1;
       }
       break;
     case 'n':
       _rtOpts->n_packets = strtol(optarg, &optarg, 0);
       break;
     case 'z':
       _rtOpts->n_threads = strtol(optarg, &optarg, 0);
       break;
     case 'f':
       _rtOpts->fake_data = 1;
       break;
     case 'x':
       _rtOpts->n_cpus = strtol(optarg, &optarg, 0);
       printf("Requested CPUs number %d\n",_rtOpts->n_cpus );
       if (_rtOpts->n_cpus > num_cores)
        {
          _rtOpts->n_cpus = num_cores;
          printf("Trying to use too many CPUs, seeting n_cpus to max=%d",_rtOpts->n_cpus );
        }
       break;
     case 'a':
       total_bytes = strtol(optarg, &optarg, 0);
       _rtOpts->n_packets =(int)total_bytes/_rtOpts->n_threads/_rtOpts->s_payload;
       break;
     case 'v':
       _rtOpts->verbose = strtol(optarg, &optarg, 0);
       printf("Verbose        : %d \n", _rtOpts->verbose);
       break;
     case 'b':
       _rtOpts->n_board = strtol(optarg, &optarg, 0);
       break;
     case 'e':
       _rtOpts->n_chan = strtol(optarg, &optarg, 0);
       break;
     case 'd':
       _rtOpts->n_sample   = strtol(optarg, &optarg, 0);
       break;
     case 'g':
       _rtOpts->rx_to_file = 1;
       break;
     case 'h':
       _rtOpts->acq_num = strtol(optarg, &optarg, 0);
       break;
     case 'j':
       _rtOpts->rx_to_fbb = strtol(optarg, &optarg, 0);
       break;
     case 'k':
       _rtOpts->check_dummy = 1;
       break;
     case 'l':
       _rtOpts->tx_read_mode =  strtol(optarg, &optarg, 0);
       break;
     case '?':
     default:
       info();
       return -1;
       break;
     }
   }
   if(!_rtOpts->fake_data)
   {
       if(rtOpts.tx_read_mode == 0)
       {
            n_boards_per_acq = _rtOpts->n_board==-1?8:1;
            n_chan_per_board = 0;

            _rtOpts->n_pkt_chan =  (_rtOpts->n_sample*2 ) / _rtOpts->s_payload; 
            if(_rtOpts->n_pkt_chan*_rtOpts->s_payload/2 < _rtOpts->n_sample)
              _rtOpts->n_pkt_chan++;
            for (i=0;i<8;i++)
                if(_rtOpts->n_chan & (1<<i))
                    n_chan_per_board++;
            _rtOpts->n_packets  = n_chan_per_board * n_boards_per_acq * _rtOpts->n_pkt_chan;
            
            
            if(_rtOpts->verbose == 3)
            {
                  printf("n_chan_per_board      : %d\n", n_chan_per_board);
                  printf("n_boards_per_acq      : %d\n", n_boards_per_acq);
                  printf("_rtOpts->n_sample     : %d\n", _rtOpts->n_sample);
                  printf("_rtOpts->n_pkt_chan   : %d\n", _rtOpts->n_pkt_chan);
                  printf("_rtOpts->n_packets    : %d\n", _rtOpts->n_packets);
            }
       }
       if(rtOpts.tx_read_mode > 1)
       {
             switch(_rtOpts->n_board) {
              case 0x1: // read first bank
                n_boards_per_acq = 4;
                break;
              case 0x2: // read second bank
                n_boards_per_acq = 4;
                break;
              case 0x3: // read both banks
              case -1:  // read both banks
                n_boards_per_acq = 8;
                break;
              default:
                 printf("ERROR: Wrong board/bank value %d\n", _rtOpts->n_board);
                 return 0;
                break;
              }
            n_chan_per_board = 8;
            //adjust payload size to to sample bundel (32 chan) boundary
            _rtOpts->s_payload =  ((int)(_rtOpts->s_payload/48))*48;
            // number of packets needed
            _rtOpts->n_pkt_chan =  (_rtOpts->n_sample*48 ) / _rtOpts->s_payload;
            // "reminder"
            if(_rtOpts->n_pkt_chan*_rtOpts->s_payload/48 < _rtOpts->n_sample)
              _rtOpts->n_pkt_chan++;
          
            _rtOpts->n_packets  = _rtOpts->n_pkt_chan;
            
            
            if(_rtOpts->verbose == 3)
            {
                  printf("n_chan_per_board      : %d\n", n_chan_per_board);
                  printf("n_boards_per_acq      : %d\n", n_boards_per_acq);
                  printf("_rtOpts->n_sample     : %d\n", _rtOpts->n_sample);
                  printf("_rtOpts->n_pkt_chan   : %d\n", _rtOpts->n_pkt_chan);
                  printf("_rtOpts->n_packets    : %d\n", _rtOpts->n_packets);
            }
       }
   }

   if(_rtOpts->verbose)  printf("------ Settings ------------\n");
   if(_rtOpts->verbose)  printf("Service        : %s \n", _rtOpts->s_type==S_SERVER?"Server":"Client");
   if(_rtOpts->verbose)  printf("Protocol       : %s \n", _rtOpts->c_type==C_UDP?"UDP":"TCP");
   if(_rtOpts->verbose)  printf("Payload        : %d bytes\n", _rtOpts->s_payload);
   if(_rtOpts->verbose)  printf("Packets        : %d\n", _rtOpts->n_packets);
   if(_rtOpts->verbose)  printf("Threads        : %d\n", _rtOpts->n_threads);
   if(_rtOpts->verbose)  printf("Save rx to file: %s\n", _rtOpts->rx_to_file?"yes":"no");
   if(_rtOpts->verbose)  printf("Copy rx to FBB : %d (0: off, 1: 16bit damples, 2: 32bit raw dat)\n", _rtOpts->rx_to_fbb);
   if(_rtOpts->verbose)  printf("CPUs           : %d\n", _rtOpts->n_cpus);
   if(_rtOpts->verbose)  printf("Dst IP addr    : %s\n", _rtOpts->ip_add);
   if(_rtOpts->verbose)  printf("FSI data       : %s\n", _rtOpts->fake_data?"fake data":"data from HW");
   if(_rtOpts->verbose)  printf("Check dummy dat: %s\n", _rtOpts->check_dummy?"yes":"yes");
   if(_rtOpts->verbose)  printf("TX read mode   : %s\n", _rtOpts->tx_read_mode==0?"unpack":"raw");
   if(_rtOpts->verbose)  printf("----------------------------\n");
   if(_rtOpts->verbose)  printf("Acq from board : %d (-1 for all)\n", _rtOpts->n_board);
   if(_rtOpts->verbose) {
                         printf("Acq from chan  : 0x%x: ", _rtOpts->n_chan);
                         for(int i=0;i<8;i++) if(_rtOpts->n_chan & (1<<i)) printf("%d ", i);
                         printf("\n");
                        }
   if(_rtOpts->verbose)  printf("Acq num samples: %d (-1 will read from hw)\n", _rtOpts->n_sample);
   if(_rtOpts->verbose)  printf("Acq number     : %d (0: immediate acq, otherwise wait for DMA)\n", _rtOpts->acq_num);
   
   return 0;
}

/*
 * Fuction to do unpack data from memory and pack into payload of packets
 */
int fsi_data_tx(void *mem, uint16_t *payload, unsigned board, unsigned channel, unsigned off, unsigned s_num)
{
   if(rtOpts.fake_data) return 0;
   /*
    * DO HERE UNPACKING
    */
   int i;
   int s_offset = off; // sample offset
   int ret=0;
   for (i=0; i<s_num; i++)
   {
       payload[i]=(uint16_t)raw_data(board, channel, s_offset, mem);
       ret = check_dummy_data(payload[i], off==0);
       if(rtOpts.verbose==4 || ret < 0) printf("Brd=%d | chan=%d | samp=%5d | val=%d\n",
                             board, channel, s_offset, payload[i]);
       s_offset++;
       

   }
   return 0;
}

/*
 * Init fake data - TODO: change if needed
 */
int fsi_data_init()
{
   // do nothing when faking it...
   if(rtOpts.fake_data) return 0;

   // get connection to config registers
   if(rtOpts.verbose==3) printf("Enter m_axi_map()\n");
   m_axi_map(M_AXI_BASE);

   //set DMA transfer lenght
   if(rtOpts.verbose==3) printf("Set number of samples (DMA transfer lenght): %d\n", rtOpts.n_sample);
//    m_axi_writel(SYS_TOP_SUB_REG_LENGTH, (uint32_t)rtOpts.n_sample);
   dma_init((uint32_t)rtOpts.n_sample);

   // enable trigger
   if(rtOpts.verbose==3)printf("Enter fsi_trig_enable()\n");
   fsi_trig_enable(1);

//    // read number of samples to be DMAed, if not specified
//    if(rtOpts.n_sample==-1)
//    {
//       rtOpts.n_sample = get_dma_length();
//       printf("Acq num samples read from hw: %d\n",rtOpts.n_sample);
// 
//    }

   //map DDR
   ddr_mem=mmap_ram(PHYSICAL_ADDR);
}

/* *****************************************************************************
 *               Transmission thread 
 * *****************************************************************************
 * Thread that sends N packets with M bytes over UDP or TCP, highlights:
 *
 * -> FSI header is added to the requested payload
 *        //data used to check transmission
 *        int thread_id;
 *        int packet_id;
 *        int payload_size;
 *        int expected_pkt_n;
 *        // possible FSI header
 *        int chan;
 *        int board;
 *        int first_sample_id;
 *        int sample_number;
 *
 * -> for UDP a "start of transmission" frame is sent to help evaluation of
 *    transmssion time
 *
 * -> time of transmission-only is measured (exluding opening socket, config, etc)
 * 
 * -> transmission parameters are passed to the main process:
 *       int id;
 *       uint64_t bytes_transferred;
 *       uint64_t pkts_transfered;
 *       uint64_t bytes_expected;
 *       uint64_t pkts_expected;
 *       double duration;
 *       uint64_t t_start;
 *       uint64_t t_end;
 *       pthread_t thread;
 */
void *worker_thread_tx(void *pst)
{
    struct thread_state *st = (struct thread_state *)pst;
    int sockfd;
    char buffer[TX_BUFFER_SIZE_BYTES];
    char *hello = "Hello from client";
    struct sockaddr_in servaddr;

    int core_id = st->id % rtOpts.n_cpus;

    cpu_set_t cpuset;
    CPU_ZERO(&cpuset);
    CPU_SET(core_id, &cpuset);

    pthread_t current_thread = pthread_self();
    pthread_setaffinity_np(current_thread, sizeof(cpu_set_t), &cpuset);
    printf("Thread affinity %d->%d\n", st->id, core_id);

    st->bytes_transferred = 0;
    st->pkts_transfered   = 0;

    /* **********************************************************************
     * Network config
     */

    memset(&servaddr, 0, sizeof(servaddr));

    // Filling server information
    servaddr.sin_family = AF_INET;
    servaddr.sin_port = htons(PORT + st->id);
    servaddr.sin_addr.s_addr = inet_addr(rtOpts.ip_add);//0x0200000a;

    //------------------------------ new approach -----------------------------
    struct addrinfo hints, *dstinfo = NULL, *srcinfo = NULL, *p = NULL;
    int rv = -1, ret = -1, len = -1,  numbytes = 0;
    memset(&hints, 0, sizeof hints);
    hints.ai_family = AF_INET;
    hints.ai_socktype = SOCK_DGRAM;
    hints.ai_protocol = IPPROTO_UDP;
    hints.ai_flags = AI_PASSIVE;

    // ------------------------------------------------------------------------
    // Creating socket file descriptor for UDP or TCP
    if(rtOpts.c_type == C_UDP)
    {
#define NEW_SOCKET 1
#ifdef NEW_SOCKET
        //------------------------------ new approach -----------------------------
        /*For destination address*/
        printf("[Thread %d] Establish socket in a new way..\n",st->id );
        if ((rv = getaddrinfo(rtOpts.ip_add, "8080", &hints, &dstinfo)) != 0) {
            fprintf(stderr, "getaddrinfo for dest address: %s\n", gai_strerror(rv));
            exit(EXIT_FAILURE);
        }

        // loop through all the results and make a socket
        for(p = dstinfo; p != NULL; p = p->ai_next) {

            if ((sockfd = socket(p->ai_family, p->ai_socktype,
                        p->ai_protocol)) == -1) {
              perror("socket");
              continue;
            }
            /*Taking first entry from getaddrinfo*/
            printf("[Thread %d] socket OK..\n",st->id );
            break;
        }
        int send_buff = 33554432;   //=20^25
        int res = setsockopt(sockfd, SOL_SOCKET, SO_SNDBUF, &send_buff, sizeof(send_buff));
        /*Failed to get socket to all entries*/
        if (p == NULL) {
            fprintf(stderr, "Failed to get socket\n");
            ret = 2;
            exit(EXIT_FAILURE);
        }
        /*Connect this datagram socket to destination address info */
        if((rv= connect(sockfd, p->ai_addr, p->ai_addrlen)) != 0) {
            fprintf(stderr, "connect: %s\n", gai_strerror(rv));
            ret = 3;
            exit(EXIT_FAILURE);
        }
        printf("[Thread %d] Connected..\n",st->id );
       // ------------------------------------------------------------------------
#endif
// #define OLD_SOCKET 1
#ifdef OLD_SOCKET
      printf("[Thread %d] Establish socket in an old way..\n",st->id );
      if ((sockfd = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP)) < 0)
      {
          perror("Socket creation failed");
          exit(EXIT_FAILURE);
      }
        /*Specifies the total per-socket buffer space reserved for sends.*/
      int send_buff = 33554432;   //=20^25
      int res = setsockopt(sockfd, SOL_SOCKET, SO_SNDBUF, &send_buff, sizeof(send_buff));

#endif
    }
    else if(rtOpts.c_type == C_TCP)
    {
        printf("[Thread %d] Do the TCP/IP magic...\n",st->id );
        int connfd;
        struct sockaddr_in cli;

        // socket create and varification
        sockfd = socket(AF_INET, SOCK_STREAM, 0);
        if (sockfd == -1) {
            printf("socket creation failed...\n");
            exit(0);
        }
        /*Specifies the total per-socket buffer space reserved for sends.*/
        int send_buff = 1048576;
        int res = setsockopt(sockfd, SOL_SOCKET, SO_SNDBUF, &send_buff, sizeof(send_buff));

        printf("[Thread %d] RV %d\n",st->id , res );

        // connect the client socket to server socket
        if (connect(sockfd, (struct sockaddr_in *)&servaddr, sizeof(servaddr)) != 0) {
            printf("[Thread %d] connection with the server failed...\n", st->id);
            exit(0);
        }
        printf("[Thread %d] connected to the server..\n", st->id);
    }
    else
    {
        printf("[Thread %d] ERROR: Wrong connection type chosen: %d\n", st->id, rtOpts.c_type);
        return 0;
    }

    int acq_num=1;
    while(1)
    {
          uint64_t trigger_time=0;
          fsi_trig_enable(1);
          if(rtOpts.acq_num != 0)
          {
             fprintf(stderr,"[Thread %d] wait DMA ",st->id);
              while(new_dma_ready() == 0)
                 usleep(100000);
              trigger_time = get_tics();
              clear_dma_ready();
              fsi_trig_enable(0); //disable trigger during acquisition
          }
          if(rtOpts.verbose >= 2) fprintf(stderr, " --> start acq %3d for ", acq_num);
          int n, len;

          /* **********************************************************************
          * transmit data
          */

          int i = 0;
          double total = 0;
          uint64_t t_start = 0;// get_tics();
          uint64_t t_end = 0;
          int pkt_h_size = sizeof(struct packet_header);
          int pkt_p_size = rtOpts.s_payload + pkt_h_size;
          struct packet_fsi pkt;
          //send a "start transmission" packate to better measure tmie
          int b_cur     = 0;       // current board id
          int b_start   = 0;
          int b_stop    = 0;
          int c_cur     = 0;       // current channel id
          int s_cur     = 0;       // current sample offset
          int s_stop    = 0;       // total number of samples per channel
          int p_cur     = 0;       // current packet being transmitted
          int p_stop    = 0;       // packets per channel
          int s_per_pkt = 0;       // samples that fit into a single frame
          int p_id      = 0;       // packet ID (total for entire transmissin)
          int s_cur_pkt = 0;       // number of samples in the current packet
          int n_boards_per_acq=0;  // how many boards are read
          int n_chan_per_board=0;  // how many channels per board



          if(rtOpts.tx_read_mode > 1)
          {
             switch(rtOpts.n_board) {
              case 0x1: // read first bank
                b_start          = 0;
                b_stop           = 0;
                n_boards_per_acq = 4;
                break;
              case 0x2: // read second bank
                b_start          = 1;
                b_stop           = 1;
                n_boards_per_acq = 4;
                break;
              case 0x3: // read both banks
              case -1:  // read both banks
                b_start          = 0;
                b_stop           = 1;
                n_boards_per_acq = 8;
                break;
              default:
                 printf("[Thread %d] ERROR: Wrong board/bank value %d\n", st->id, rtOpts.n_board);
                 return 0;
                break;
              }
              /*
               * In this case we send samples from 4 boards (1 bank) and 8 channels.
               * So 1 sample is actually 4 * 8 * 12 bits = 48 bytes = 12 x 32-bit values
               * We transfer entire bank, so 
               * 
               */
              n_chan_per_board = 8;
              s_stop           = rtOpts.n_sample;     // number of samples (per chan)
              p_stop           = rtOpts.n_packets;    // packets per bank
              s_per_pkt        = rtOpts.s_payload/48; // samples that fit into a single frame

          }
          else
          {
              b_start   = rtOpts.n_board==-1?1:rtOpts.n_board;
              b_stop    = rtOpts.n_board==-1?8:rtOpts.n_board;
              s_stop    = rtOpts.n_sample;       // total number of samples per channel
              p_stop    = rtOpts.n_pkt_chan;     // packets per channel
              s_per_pkt = rtOpts.s_payload/2;    // samples that fit into a single frame

               for (i=0;i<8;i++)
                  if(rtOpts.n_chan & (1<<i))
                      n_chan_per_board++;
               n_boards_per_acq = rtOpts.n_board==-1?8:1;

          }

          if(rtOpts.c_type == C_UDP)
          {
              // in the fist massage state some basic info
              if(rtOpts.tx_read_mode == 2)// needed for reception of raw data into buffer directl
                 pkt.header.payload_size = rtOpts.s_payload;
              pkt.header.chan            = n_chan_per_board; // numb of acq chans per board
              pkt.header.board           = n_boards_per_acq;  // from how many boards
              pkt.header.sample_number   = rtOpts.n_sample;   // how many samples per chan
              int rv = sendto(sockfd, (const char *)&pkt, pkt_h_size,
                                    MSG_CONFIRM, (const struct sockaddr *)&servaddr,
                                    sizeof(servaddr));
          }
          //                          real data
          ///////////////////////////////////////////////////////////////////////
          // measure start
          st->t_start = get_tics();
          if(rtOpts.fake_data)
          {
          ///////////////////////////////////////////////////////////////////////
          //                          fake data
          ///////////////////////////////////////////////////////////////////////
              for (i = 0; i < rtOpts.n_packets; i++)
              {
                  pkt.header.thread_id       = st->id;
                  pkt.header.packet_id       = i;
                  pkt.header.payload_size    = rtOpts.s_payload;
                  pkt.header.expected_pkt_n  = rtOpts.n_packets;
                  pkt.header.chan            = rtOpts.n_chan;
                  pkt.header.board           = rtOpts.n_board;
                  pkt.header.first_sample_id = i*rtOpts.s_payload/2;
                  pkt.header.sample_number   = rtOpts.s_payload*2;

                  int rv = sendto(sockfd, (const char *)&pkt, pkt_p_size,
                                  MSG_CONFIRM, (const struct sockaddr *)&servaddr,
                                  sizeof(servaddr));

                  total += rv;
              }

          }
          else if(rtOpts.tx_read_mode == 0) // unpack data
          {


              for (b_cur=b_start; b_cur<=b_stop;b_cur++) // for each board to be read
              {
                  fprintf(stderr, " board %d, chans: ",  b_cur);
                  for (c_cur=0; c_cur<8;c_cur++)         // for each channel
                  {
                      if(rtOpts.n_chan & (1<<c_cur))  // that is to be read
                      {
                            fprintf(stderr, "%d,", c_cur);
                            s_cur = 0;
                            for (p_cur=0;p_cur<p_stop;p_cur++) // send a number of pakets
                            {
                                  if(p_cur==(p_stop-1)) // last packet
                                    s_cur_pkt               = rtOpts.n_sample - s_cur;
                                  else
                                    s_cur_pkt               = s_per_pkt;
                                  if(rtOpts.verbose>=3) printf("[Thread %d] TX packet : %3d for b%d c%d (pkt_id=%3d, samples %7d to %7d)\n",
                                      st->id, p_cur, b_cur, c_cur, p_id, s_cur, s_cur+s_cur_pkt);
                                  pkt.header.thread_id       = st->id;
                                  pkt.header.packet_id       = p_id++;
                                  pkt.header.expected_pkt_n  = rtOpts.n_packets;
                                  pkt.header.payload_size    = s_cur_pkt*2;
                                  pkt.header.chan            = c_cur;
                                  pkt.header.board           = b_cur;
                                  pkt.header.first_sample_id = s_cur;
                                  pkt.header.sample_number   = s_cur_pkt;
                                  fsi_data_tx(ddr_mem, pkt.payload, b_cur, c_cur, s_cur, s_cur_pkt);

                                  pkt_p_size = s_cur_pkt*2 + pkt_h_size;
                                  int rv = sendto(sockfd, (const char *)&pkt, pkt_p_size,
                                                  MSG_CONFIRM, (const struct sockaddr *)&servaddr,
                                                  sizeof(servaddr));

                                  total += rv;
                                  s_cur = s_cur+s_cur_pkt;
                            }
                      }
                  }
              }
//               cmd_fsi_regs();
          }
          else if(rtOpts.tx_read_mode == 1) // send raw without a header
          {

              for(b_cur = b_start; b_cur<=b_stop; b_cur++)
              {
                 uint32_t *samp =0;
                 s_cur = 0;
                 p_id = 0;
                 if(rtOpts.verbose==3) printf("[Thread %d] Sending %d samples for bank %d, expected packet number: %d \n",
                                       st->id, s_stop, b_cur, rtOpts.n_packets);
                 
                 if(b_cur == 0)
                   samp = (uint32_t *)(ddr_mem + DMA0_BUF_ADDR);
                 else
                   samp = (uint32_t *)(ddr_mem + DMA1_BUF_ADDR);

                 for(s_cur = 0; s_cur < s_stop; s_cur+=s_per_pkt )
                 {
                    if(s_cur+s_per_pkt<s_stop)
                       pkt_p_size = s_per_pkt*48;
                    else
                      pkt_p_size = (s_stop-s_cur)*48;

                    int rv = sendto(sockfd, (const char *)&samp[s_cur*12], pkt_p_size,
                             MSG_CONFIRM, (const struct sockaddr *)&servaddr,
                                                 sizeof(servaddr));
//                     int rv=send(sockfd, (const char *)&samp[s_cur*12], pkt_p_size,  0);
                    total += rv;
                    if(rtOpts.verbose==3) printf("[Thread %d] TX packet %3d of %d Bytes for bank %d (samples %7d to %7d, total tx %.3f MBytes), \n",
                                      st->id, p_id, rv, b_cur, s_cur, s_cur+s_per_pkt,(double)total/MB_factor);
                    p_id++;
                }
              }
          }
          else if(rtOpts.tx_read_mode == 2) // send raw with the header
          {


              for(b_cur = b_start; b_cur<=b_stop; b_cur++)
              {
                 uint32_t *samp =0;
                 s_cur = 0;
                 p_id = 0;
                 int iovcnt;
                 struct iovec iov[2];
                 struct packet_header h_cur;
                 iov[0].iov_base = &h_cur;
                 iov[0].iov_len  = sizeof(h_cur);

                 if(rtOpts.verbose >= 2) printf("[Thread %d] Sending %d samples for bank %d, expected packet number: %d \n",
                                       st->id, s_stop, b_cur, rtOpts.n_packets);

                 if(b_cur == 0)
                   samp = (uint32_t *)(ddr_mem + DMA0_BUF_ADDR);
                 else
                   samp = (uint32_t *)(ddr_mem + DMA1_BUF_ADDR);

                 for(s_cur = 0; s_cur < s_stop; s_cur+=s_per_pkt )
                 {
                    if(s_cur+s_per_pkt<s_stop)
                       pkt_p_size = s_per_pkt*48;
                    else
                      pkt_p_size = (s_stop-s_cur)*48;

                    h_cur.thread_id       = st->id;
                    h_cur.packet_id       = p_id;
                    h_cur.expected_pkt_n  = rtOpts.n_packets;
                    h_cur.payload_size    = pkt_p_size;
                    h_cur.chan            = 0xff;
                    h_cur.board           = b_cur;
                    h_cur.first_sample_id = s_cur;
                    h_cur.sample_number   = pkt_p_size/48;

                    iov[1].iov_base = &samp[s_cur*12];
                    iov[1].iov_len  = pkt_p_size;

                    iovcnt = sizeof(iov) / sizeof(struct iovec);

                    int rv=writev(sockfd, iov, iovcnt);
//                     int rv = sendto(sockfd, (const char *)&samp[s_cur*12], pkt_p_size,
//                              MSG_CONFIRM, (const struct sockaddr *)&servaddr,
//                                                  sizeof(servaddr));
                    total += rv;
                    if(rtOpts.verbose==3) printf("[Thread %d] TX packet %3d of %d Bytes (h:%d, p:%d) for bank %d"
                      "(samples %7d to %7d, total tx %.3f MBytes), \n",
                       st->id, p_id, rv, (int)iov[0].iov_len , (int)iov[1].iov_len ,b_cur, s_cur, s_cur+s_per_pkt,(double)total/MB_factor);
                    p_id++;
                }
              }
          }
          st->t_end = get_tics();
          // measure stop
          ///////////////////////////////////////////////////////////////////////
          st->duration = (double)(st->t_end - st->t_start) / 1e6;
          st->bytes_transferred = total;
          st->pkts_transfered   = i;
          st->bytes_expected    = rtOpts.n_packets*pkt_p_size;
          st->pkts_expected     = rtOpts.n_packets;

          double total_MBps          = (double)total / st->duration / MB_factor;
          double total_Gbps          = (total_MBps*8.0)/1024.0;
          double total_chan          =n_chan_per_board*n_boards_per_acq;
          double fsi_data = total - i*pkt_h_size;
          fprintf(stderr, "--> finished: tx- ed %lld bytes (%.1f MBytes) in %d pkts for %d channels in %.3f s (%.3f Gbps, %.3f chan/s)",
              (long long int)fsi_data, 
              (double)fsi_data/MB_factor,
              p_id,
              (int)total_chan,
               st->duration,
              total_Gbps,
              (double)st->duration/total_chan
                 );
          if(!rtOpts.fake_data)
          {
              fprintf(stderr, "[ ");
              int d;
              int dma_banks=2;
              for (d = 0; d < dma_banks; d++)
              {
                  unsigned addr = SYS_TOP_SUB_REG_CONFIGS + (d * SYS_TOP_SUB_REG_CONFIGS_SIZE);
                  uint32_t ret = m_axi_readl(addr + SYS_TOP_SUB_REG_CONFIGS_STATUS);
                  fprintf(stderr, "DMA %d FIFO status(0x%x): max=%2d, err=%2d", d,ret,
                    (int)((ret & SYS_TOP_SUB_REG_CONFIGS_STATUS_FIFO_MAX_MASK ) >> SYS_TOP_SUB_REG_CONFIGS_STATUS_FIFO_MAX_SHIFT),
                    (int)((ret & SYS_TOP_SUB_REG_CONFIGS_STATUS_FIFO_ERRS_MASK) >> SYS_TOP_SUB_REG_CONFIGS_STATUS_FIFO_ERRS_SHIFT));
                  if(d==dma_banks-1)
                    fprintf(stderr, "]");
                  else
                    fprintf(stderr, " | ");
              }
          }
          
          if(rtOpts.acq_num == 0 || (acq_num > 0 && acq_num == rtOpts.acq_num))
            break;
          else
            acq_num++;
          
//           fprintf(stderr, "\n");
          uint64_t now_time =0;
          uint64_t diff = 0;
          uint64_t dead_time_s = 4;
          while (get_tics() - trigger_time < dead_time_s* 1000000ULL)
          {
            fprintf(stderr, ".");
            usleep(100000);
          }

//           do{
//             now_time = get_tics();
//             diff     = now_time - trigger_time;
// //             fprintf(stderr, "start = %ld, stop=%ld, diff=%ld\n",trigger_time,now_time,diff);
//             usleep(10000);// poll every 10ms
//           } while(diff < 3000000ULL);
          fprintf(stderr, "\n");

    }
    close(sockfd);
    return NULL;
}

/* *****************************************************************************
 *               Reception thread
 * *****************************************************************************
 * Thread that receives N packets with M bytes over UDP or TCP, highlights:
 *
 * -> FSI header allows to know how many packets and how much data is expected
 *        //data used to check transmission
 *        int thread_id;
 *        int packet_id;
 *        int payload_size;
 *        int expected_pkt_n;
 *        // possible FSI header
 *        int chan;
 *        int board;
 *        int first_sample_id;
 *        int sample_number;
 *
 * -> for UDP a "start of transmission" frame is received to help evaluation of
 *    transmssion time, also after it's reception a transmission timeout is 
 *    enabled so that the thread can complete even if some packets are lost.
 *
 * -> time of reception-only is measured (exluding opening socket, config, etc)
 * 
 * -> reception parameters are passed to the main process:
 *       int id;
 *       uint64_t bytes_transferred;
 *       uint64_t pkts_transfered;
 *       uint64_t bytes_expected;
 *       uint64_t pkts_expected;
 *       double duration;
 *       uint64_t t_start;
 *       uint64_t t_end;
 *       pthread_t thread;
 */
void *worker_thread_rx(void *pst)
{
    struct thread_state *st = (struct thread_state *)pst;
    uint64_t rx_bytes=0; 
    int sockfd, connfd, len;
    struct sockaddr_in servaddr, cli;
    char buf[RX_BUFFER_SIZE_BYTES];
    int core_id = st->id % rtOpts.n_cpus;
    int pkt_h_size = sizeof(struct packet_header);
    int pkt_n=0;
    uint64_t t_end;
    uint64_t t_start; 
    struct packet_header pkt_h;
    int n = 0;
   
    int file_id=0;
    char filename[30];

    st->bytes_transferred = 0;
    st->pkts_transfered   = 0;

    cpu_set_t cpuset;
    CPU_ZERO(&cpuset);
    CPU_SET(core_id, &cpuset);

    pthread_t current_thread = pthread_self();
    pthread_setaffinity_np(current_thread, sizeof(cpu_set_t), &cpuset);
    if(rtOpts.verbose==3) printf("Thread affinity %d->%d\n", st->id, core_id);
    sockfd = -1;

    // socket create and verification
    if(rtOpts.c_type == C_UDP)
        sockfd = socket(AF_INET, SOCK_DGRAM, 0);
    else if(rtOpts.c_type == C_TCP)
        sockfd = socket(AF_INET, SOCK_STREAM, 0);
    else
        printf("ERROR: Wrong connection type chosen: %d\n", rtOpts.c_type);

    if (sockfd == -1)
    {
        perror("socket creation failed");
        exit(EXIT_FAILURE);
    }
   if(rtOpts.verbose==3)  printf("socket OK for thread %d\n",st->id);
   /*
    * Allows the socket to be bound to an address that is already in use. 
    * For more information, see bind. Not applicable on ATM sockets.
    */
   if (setsockopt(sockfd, SOL_SOCKET, SO_REUSEADDR, &(int){1}, sizeof(int)) < 0)
      printf("setsockopt(SO_REUSEADDR) failed");

    /*
     * Specifies the total per-socket buffer space reserved for receives.
     */
//     int recv_buff = 1048576;  //=2^20
    int recv_buff = 33554432; //=20^25
    int res = setsockopt(sockfd, SOL_SOCKET, SO_RCVBUF, &recv_buff, sizeof(recv_buff));
    if(rtOpts.verbose==3) printf("RV %d\n", res );

    bzero(&servaddr, sizeof(servaddr));

    // assign IP, PORT
    servaddr.sin_family = AF_INET;
    servaddr.sin_addr.s_addr = htonl(INADDR_ANY);
    servaddr.sin_port = htons(PORT + st->id);

    // Binding newly created socket to given IP and verification
    if ((bind(sockfd, (struct sockaddr *)&servaddr, sizeof(servaddr))) != 0)
    {
        printf("[thread %d] socket bind failed...\n",st->id);
        exit(0);
    }
    if(rtOpts.verbose==3) printf("[thread %d] bind OK \n",st->id);
    len = sizeof(cli);
    int acq_num=1;


    while(1)
    {
          FILE *fptr;
          int lost_packets       = 0;
          int lost_packets_total = 0;
          int b_cur              = 0; // current board
          int c_cur              = 0; // current channel
          int s_start_cur        = 0; // current start sample 
          int b_per_acq          = 0; // boards per acquisition
          int c_per_board        = 0; // channels per baord
          int s_per_ch           = 0; // samples per channel
          int s_per_pkt          = 0; // samples per packet
          int i;
          int expected_payload  = 0; 
          int is_raw_data       = 0;
          
          if(rtOpts.rx_to_fbb == 1) // samples
          {
            fbb_16bw = malloc(sizeof(uint16_t)*FBB_SIZE_16BW);
            if(fbb_16bw == NULL)
            {
              printf("[thread %d] FBB not allocated\n",st->id);
              exit(0);
            }
          }
          if(rtOpts.rx_to_fbb == 2) // raw data
          {
            fbb_32bw = malloc(sizeof(uint32_t)*FBB_SIZE_32BW);
            if(fbb_32bw == NULL)
            {
              printf("[thread %d] FBB not allocated\n",st->id);
              exit(0);
            }
          }


          pkt_n = 0;
          rx_bytes=0;

          if(rtOpts.c_type == C_UDP)
          {
              struct timeval tv;
              tv.tv_sec = 5;
              tv.tv_usec = 100000;

              if(rtOpts.verbose==3) printf("[thread %d] Wait for UDP packets\n",st->id);

              // wait for "start transmission packet"
              n = recvfrom(sockfd, buf, sizeof(buf), 0, (struct sockaddr*)&cli,&len);
              // set timeout
              // TODO: causes crash, check later
              // if (setsockopt(sockfd, SOL_SOCKET, SO_RCVTIMEO,&tv,sizeof(tv)) < 0) 
              // {
              //   printf("[thread %d] timeout waiting for the start frame\n",st->id);
              //   continue;
              // }
              struct packet_fsi *pkt = (struct packet_fsi*)buf;

              b_cur        = -1;
              c_cur        = -1;
              s_per_ch     = pkt->header.sample_number;
              if(rtOpts.rx_to_fbb == 2)
                 is_raw_data  = 1;
              c_per_board  = pkt->header.chan;
              b_per_acq    = pkt->header.board;

              expected_payload = pkt->header.payload_size;
              if (n>0) if(rtOpts.verbose>=2) printf("[thread %d] Received start frame"
                    " (samp per chan: %d, chan per board: %d, boards per acq: %d, expected payload: %d, is raw data: %s)\n",
                      st->id,s_per_ch, c_per_board,b_per_acq,expected_payload, is_raw_data?"yes":"no");
              //////////////
              // crate file

              if(rtOpts.rx_to_file || rtOpts.rx_to_fbb)
              {

                  time_t t     = time(NULL);
                  struct tm tm = *localtime(&t);
                  sprintf(filename, "fsi_acq_%d-%d-%d-%dh%dm%ds",
                  tm.tm_year+1900, tm.tm_mon+1, tm.tm_mday,tm.tm_hour, tm.tm_min, tm.tm_sec);
              }

              ///////////////////////////////////////////////////////////////////////
              // measure start
              st->t_start = get_tics();
              if(rtOpts.verbose==3) printf("[thread %d] Start reception\n",st->id);

              ssize_t bytes_read;
              int fd;
              int iovcnt;
              int start_offset= 0;                  // if bank 0 starts at this address
              if(b_per_acq==1)   
                start_offset  = (s_per_ch*8*4)*12;  // if bank 1 starts at this address
              struct iovec iov[2];
              struct packet_header h_cur;
              iov[0].iov_base = &h_cur;
              iov[0].iov_len  = sizeof(h_cur);
              iov[1].iov_base = &fbb_32bw[start_offset];
              iov[1].iov_len  = expected_payload;
              iovcnt = sizeof(iov) / sizeof(struct iovec);

              while(1)
              {
                  lost_packets = 0;
                  struct packet_fsi *pkt;
                  
                  if(rtOpts.rx_to_fbb == 2)
                  {
                    n = readv(sockfd, iov, iovcnt);
                    //preapre for next acquisitin
                    start_offset = start_offset + h_cur.sample_number*12;
                    iov[1].iov_base = &fbb_32bw[start_offset];
                  }
                  else
                  {
                    n = recvfrom(sockfd, buf, sizeof(buf), 0, (struct sockaddr*)&cli,&len);
                    pkt   = (struct packet_fsi*)buf;
                    memcpy(&h_cur, &pkt->header, pkt_h_size);
                  }
                  s_start_cur        = h_cur.first_sample_id;
                  s_per_pkt          = h_cur.sample_number;
                  // handlnig packet loss
                  lost_packets       = h_cur.packet_id-pkt_n;
                  lost_packets_total = lost_packets_total+lost_packets;

                  if(rtOpts.verbose==3) printf("[thread %d] Rx-ed UDP %d with pkt_id %d size %d, expected %d pakcets\n",
                    st->id, pkt_n, h_cur.packet_id, n,h_cur.expected_pkt_n);

                  if(lost_packets && rtOpts.rx_to_file) // lost packets
                  {
                       printf("[thread %d] Lost %d packets\n",st->id, lost_packets);
                       if(b_cur == h_cur.board || c_cur == h_cur.chan)
                       {
                         for(i=0; i<lost_packets;i++)
                            write_to_file_only_values_zeros(fptr, pkt_n+i,b_cur, c_cur,s_per_pkt);
                         lost_packets = 0;
                       }
                  }


//                   if(rtOpts.rx_to_file)
//                   {
//                     if (b_cur != pkt->header.board || c_cur != pkt->header.chan)
//                     {
//                       if(rtOpts.verbose==3) printf("[thread %d] New Board, Chan\n",st->id);
//                       // if(fptr && pkt_n > 0) fclose(fptr); // TODO causes craches
//                       b_cur = pkt->header.board;
//                       c_cur = pkt->header.chan;
//                       fptr = create_file_per_chan(b_cur, c_cur, s_per_ch, filename);
//                       if(lost_packets)
//                       {
//                          for(i=0; i<pkt->header.packet_id;i++)
//                            write_to_file_only_values_zeros(fptr, i,b_cur, c_cur,s_per_ch);
//                          lost_packets = 0;
//                       }
//                     }
//                     else
//                       write_to_file_only_values(fptr,pkt);
//                   }
                  if (b_cur != h_cur.board || c_cur != h_cur.chan)
                  {
                    if(rtOpts.verbose==3) printf("[thread %d] New Board, Chan\n",st->id);
                    // if(fptr && pkt_n > 0) fclose(fptr); // TODO causes craches
                    b_cur = h_cur.board;
                    c_cur = h_cur.chan;

                    if(rtOpts.rx_to_file)
                       fptr = create_file_per_chan(b_cur, c_cur, s_per_ch, filename);

                    if(lost_packets)
                    {
                       for(i=0; i<h_cur.packet_id;i++)
                       {
                         if(rtOpts.rx_to_file)
                           write_to_file_only_values_zeros(fptr, i,b_cur, c_cur,s_per_pkt);
                         if(rtOpts.rx_to_fbb == 1)
                           memset(&fbb_16bw[(b_cur*N_CHAN+c_cur)*N_MAX_SAMPLES+s_start_cur], 0, sizeof(uint16_t)*s_per_pkt);
                         if(rtOpts.rx_to_fbb == 2)
                           printf("[thread %d] TODO: handle lost frames\n",st->id);

                       }
                       lost_packets = 0;
                    }
                  }
                  if(rtOpts.rx_to_file)
                      write_to_file_only_values(fptr,pkt);
                  if(rtOpts.rx_to_fbb == 1)
		  {
                     memcpy(&fbb_16bw[(b_cur*N_CHAN+c_cur)*N_MAX_SAMPLES+s_start_cur], pkt->payload, sizeof(uint16_t)*s_per_pkt);
#ifndef FSI_MAIN
		     copy_to_gpu(pkt, &gpu_addr);
#endif /* FSI_MAIN */
		  }
//                   if(rtOpts.rx_to_fbb == 1)
//                      memcpy(&fbb_32bw[s_start_cur*12], , sizeof(uint16_t)*s_per_pkt);


                  if (pkt_n == 0)
                     memcpy(&pkt_h, buf , pkt_h_size);


                  rx_bytes+=n;
                  pkt_n      = h_cur.packet_id;
                  pkt_n++;
//                   if(rtOpts.verbose==3) printf("[thread %d] Rx-ed UDP %d with pkt_id %d size %d, expected %d pakcets\n",
//                     st->id, pkt_n, pkt->header.packet_id, n,pkt_h.expected_pkt_n);
                  if (h_cur.packet_id == h_cur.expected_pkt_n - 1) 
                      break;
                  if (n < 0)
                      break;
              }
              st->t_end = get_tics();
              // measure stop
              ///////////////////////////////////////////////////////////////////////
              if(rtOpts.rx_to_file) 
                if(fptr) fclose(fptr);
          }
          else if(rtOpts.c_type == C_TCP)
          {
              // Now server is ready to listen and verification
              if ((listen(sockfd, 5)) != 0)
              {
                  printf("[thread %d] Listen failed...\n",st->id);
                  exit(0);
              }
              else
                  printf("[thread %d] Server listening @ port %d..\n", st->id, PORT + st->id);

              // Accept the data packet from client and verification
              connfd = accept(sockfd, (struct sockaddr *)&cli, &len);
              if (connfd < 0)
              {
                  printf("[thread %d] server accept failed...\n",st->id);
                  exit(0);
              }
              else
                  printf("[thread %d] server accept the client...\n", st->id);

              //////////////
              // crate file
              if(rtOpts.rx_to_file)
              {
                  struct packet_fsi *pkt = (struct packet_fsi*)buf;
                  fptr=create_file(file_id++, pkt);
              }

              ///////////////////////////////////////////////////////////////////////
              // measure start
              st->t_start = get_tics();
              // Function for chatting between client and server
              for (;;)
              {
                  n = recv(connfd, buf, sizeof(buf), 0);
                  if (pkt_n == 0)
                      memcpy(&pkt_h, buf , pkt_h_size);
      // TODO: write to file
      //             if(rtOpts.rx_to_file)
      //             {
      //                struct packet_fsi *pkt = (struct packet_fsi*)buf;
      //                write_to_file(fptr,pkt);
      //             }
                  rx_bytes+=n;
                  pkt_n++;
                  if (n <= 0)
                      break;
              }
              st->t_end = get_tics();
              // measure stop
              ///////////////////////////////////////////////////////////////////////
              printf("[thread %d] connection closed...\n", st->id);

              // After chatting close the socket
//               if(rtOpts.rx_to_file) 
//                 fclose(fptr);
              close(connfd);
          }
          
          //check reception
          int      expected_packets   = pkt_h.expected_pkt_n;
          int      fsi_payload        = pkt_h.payload_size;
          int      pkt_payload        = pkt_h.payload_size + pkt_h_size;
//           uint64_t expected_all_bytes = expected_packets * pkt_payload;
          uint64_t expected_all_bytes = 0;
          if(is_raw_data)
            expected_all_bytes = expected_packets * pkt_h_size +   // headers
                                 (b_per_acq*c_per_board*s_per_ch*12)/8; // data
          else 
            expected_all_bytes = expected_packets * pkt_h_size +   // headers
                                 b_per_acq*c_per_board*s_per_ch*2; // data

          if(rtOpts.verbose==3) printf("[Thread %d] Expected data size: %lld\n", st->id, (long long int)expected_all_bytes);
          if(rtOpts.verbose==3) printf("[Thread %d] Received data size: %lld\n", st->id, (long long int)rx_bytes);

          // if TCP, calculate the theoretical number of packets
          if(rtOpts.c_type == C_TCP)
              pkt_n = rx_bytes / pkt_payload;

          //report thread performance
          st->bytes_transferred += rx_bytes;
          st->pkts_transfered   += pkt_n;
          st->bytes_expected    += expected_packets*pkt_payload;
          st->pkts_expected     += expected_packets;
          st->duration          += (double)(st->t_end - st->t_start) / 1e6;
          double fsi_data        = rx_bytes - pkt_n*pkt_h_size;
          if(rtOpts.verbose>=1)
            fprintf(stderr, "[Thread %d] acq=%3d: rx=ed %lld  [ %.1f MB] in %.3f s! Lost packets %d, saved to %s***.dat\n",
                  st->id,acq_num, (long long int)rx_bytes,
//                   (long long int)expected_all_bytes,
                  rx_bytes/MB_factor,
                  (double)(st->t_end - st->t_start) / 1e6, lost_packets_total, filename);
          if(rtOpts.acq_num == 0 || (acq_num > 0 && acq_num == rtOpts.acq_num))
            break;
          else
            acq_num++;
            
          if(rtOpts.rx_to_fbb == 1)
          {
             int chan = 0;
             for (i=0;i<8;i++)
             {
               if(b_per_acq > 1)
                  printf("[Thread %d] rx_to_fbb does not support acquisition from more"
                  " than one board, writing data from the last transmitted\n",st->id);
               if(rtOpts.n_chan & (1<<i))
               {
                 int offset = (pkt_h.board*N_CHAN+chan)*N_MAX_SAMPLES;
                 fptr=create_file_per_chan(pkt_h.board, i, s_per_ch, filename);
                 write_to_file_entire_chan(fptr, &fbb_16bw[offset], pkt_h.board, chan,s_per_ch);
                 fclose(fptr);
                 chan++;
               }
             }
          }
          if(rtOpts.rx_to_fbb == 2)
          {
              printf("[Thread %d] TODO: do something with the data...\n",st->id);
          }
    }
    return 0;
}

/* *****************************************************************************
 *               Main code
 * *****************************************************************************
 * 1) take arguments/configs
 * 2) init fake data
 * 1) span the number of threads (rx or tx) needed
 * 2) wait for them to complete
 * 3) analyse data and provide stats
 */
int fsi_main(int argc, char *argv[])
{
    //take in arguments
    if(startup(argc,argv,&rtOpts)<0) 
        return -1;

    if(rtOpts.fake_data == 0 && rtOpts.n_threads != 1)
    {
        printf("Multithread not supported for real acquisition (non-fake) - sorry...\n");
        return -1;
    }

    struct thread_state thr[16];
    int i;

    // initialize hw (only on the client)
    if(rtOpts.s_type == S_CLIENT)
        fsi_data_init();

    // for server, listen continously
    server_listen:

    //span the number of threads (rx or tx) needed
    if(rtOpts.verbose>=1) printf("Running with %d threads distributed over %d CPUs, FSI header size: %d\n", 
           rtOpts.n_threads, rtOpts.n_cpus, (int)sizeof(struct packet_header));
    for (i = 0; i < rtOpts.n_threads; i++)
    {
        thr[i].id = i;
        if(rtOpts.s_type == S_SERVER)
          pthread_create(&thr[i].thread, NULL, worker_thread_rx, &thr[i]);
        if(rtOpts.s_type == S_CLIENT)
          pthread_create(&thr[i].thread, NULL, worker_thread_tx, &thr[i]);
    }

    //wait for them to complete
    for (i = 0; i < rtOpts.n_threads; i++)
    {
        pthread_join(thr[i].thread, NULL);
    }

    //analyse data and provide stats
    uint64_t total_bytes    = 0;
    int      total_pkts     = 0;
    double   total_time     = 0.0;
    uint64_t expected_bytes = 0;
    int      expected_pkts  = 0;
    uint64_t t_start        = thr[0].t_start;
    uint64_t t_end          = thr[0].t_end;

    for (i = 0; i < rtOpts.n_threads; i++)
    {
        total_bytes    += thr[i].bytes_transferred;
        total_pkts     += thr[i].pkts_transfered;
        total_time     += thr[i].duration;
        expected_bytes += thr[i].bytes_expected;
        expected_pkts  += thr[i].pkts_expected;
        //find the earlierst/latest time
        if (t_start > thr[i].t_start) t_start = thr[i].t_start;
        if (t_end   < thr[i].t_end)   t_end   = thr[i].t_end;
    }


    int    pkt_h_size          = sizeof(struct packet_header);
    double dt                  = (double)(t_end - t_start) / 1e6;
    double total_MBytes        = (double)total_bytes / MB_factor;
    double expected_MBytes     = (double)expected_bytes / MB_factor;
    double total_MBps          = (double)total_bytes / dt / MB_factor;
    double total_Gbps          = (total_MBps*8.0)/1024.0;
    double avg_MBps            = (((double)total_bytes / total_time)*(double)rtOpts.n_threads) / MB_factor;
    double avg_Gbps            = (avg_MBps*8.0)/1024.0;
    uint64_t total_FSI         = total_bytes - total_pkts*pkt_h_size;
    uint64_t expected_FSI      = expected_bytes - expected_pkts*pkt_h_size;
    double total_FSI_MBytes    = (double)total_FSI / MB_factor;
    double expected_FSI_MBytes = (double)expected_FSI / MB_factor;
    double total_FSI_MBps      = (double)total_FSI / dt / MB_factor;
    double total_FSI_Gbps      = (total_FSI_MBps*8.0)/1024.0;
    double avg_FSI_MBps        = (((double)total_FSI / total_time)*(double)rtOpts.n_threads) / MB_factor;
    double avg_FSI_Gbps        = (avg_FSI_MBps*8.0)/1024.0;
    double total_loss          = 100*((double)(expected_bytes - total_bytes))/(double)expected_bytes;
    double FSI_loss            = 100*((double)(expected_FSI   - total_FSI  ))/(double)expected_FSI;
    uint64_t total_pkg_loss    = expected_pkts-total_pkts;
    uint64_t FSI_data_loss     = expected_FSI   - total_FSI;
    uint64_t FSI_expected_payload=expected_FSI/expected_pkts;

    if(rtOpts.verbose==1)
    {
        printf("\n\n===================================== SUMMARY=====================================\n\n");
        printf("Total pkt payload: %.3f MBytes in %.3f s = %.0f Mbytes/sec = %.2f Gbsp (avg: %.0f Mbytes/sec = %.2f Gbsp) @ %.2f %% loss (lost %d pkts)\n",
              total_MBytes,     dt, total_MBps,     total_Gbps,     avg_MBps,     avg_Gbps,     total_loss, (int)total_pkg_loss);
        printf("Total FSI payload: %.3f MBytes in %.3f s = %.0f Mbytes/sec = %.2f Gbsp (avg: %.0f Mbytes/sec = %.2f Gbsp) @ %.2f %% loss (lost %d MBytes)\n",
              total_FSI_MBytes, dt, total_FSI_MBps, total_FSI_Gbps, avg_FSI_MBps, avg_FSI_Gbps, FSI_loss,   (int)FSI_data_loss);

        printf("\n\n==================================================================================\n\n");
    }
    if(rtOpts.verbose==0)
    {
      printf("%s %s: Threads/CPUs=%1d/%1d, FSI payload=%5d bytes in packets=%5d, total pkt data=%lld bytes (%.3f MBs) "
             "%s in %.3f s = %.2f Gbsp (avg: %.2f Gbsp) @ %.2f %% loss (lost %d pkts)\n",
              (rtOpts.c_type==C_UDP?"UDP":"TCP"),
              (rtOpts.s_type==S_SERVER?"Server":"Client"),
              rtOpts.n_threads,
              rtOpts.n_cpus,
              (int)FSI_expected_payload,
              (int)(expected_pkts/rtOpts.n_threads),
              (long long int)total_bytes,
              total_MBytes,
              (rtOpts.s_type==S_SERVER?"rx":"tx"),
              dt,
              total_Gbps,
              avg_Gbps,
              total_loss,
              (int)total_pkg_loss);
    }

    /*if(rtOpts.s_type == S_SERVER)
    {
      sleep(2);
      goto server_listen;
    }
    */
    return 0;
}

#ifdef FSI_MAIN
int main(int argc, char *argv[])
{
	return fsi_main(argc, argv);
}
#endif /* FSI_MAIN */
