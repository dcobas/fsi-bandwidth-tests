# requires gcc-aarch64-linux-gnu package in ubuntu/debian or path to your 
# crosscompiler
CC_arm=aarch64-linux-gnu-gcc
CC_host=gcc

server="cs-774-fsidev:~/FSI-trans/"
board="root@diot-sb-fsi:/usr/bin/"

${CC_arm}  fsi_bandwidth_test.c -o fsi_trans_b -lpthread
${CC_host} fsi_bandwidth_test.c -o fsi_trans_s -lpthread

#${CC} fsi_bandwidth_test.c -o fsi_bandwidth_test -lpthread
#${CC} udp-test.c -o udp-test -lpthread
#gcc tcp-server-test.c -o tcp-server-test -lpthread
#${CC} tcp-client-test.c -o tcp-client-test -lpthread
#scp tcp-client-test udp-test root@zcu102-morten:/root
scp fsi_trans_b $board
scp fsi_trans_s $server