SM := 86

CC := gcc
NVCC := /usr/local/cuda/bin/nvcc

CFLAGS = -std=c99 -g
NVCCFLAGS = -c -g --compiler-options '-fPIC'

GENCODE_FLAGS = -arch=sm_$(SM)
CUDA_LDLIBS = -lcudart -lpthread
LDLIBS = -lpthread

BUILDDIR ?= build
$(shell [ -d $(BUILDDIR) ] || mkdir $(BUILDDIR))

TARGET = fsi_bandwidth
LIB=libfsibandwidth

all: fsi_bandwidth_test solib
solib:	$(BUILDDIR)/$(LIB).so

fsi_bandwidth_test: CFLAGS+=-DFSI_MAIN
fsi_bandwidth_test: $(BUILDDIR)/fsi_bandwidth_test.o  $(BUILDDIR)/copy_to_gpu.o
	$(CC) $(CFLAGS) $^ -o $@ $(LDLIBS) $(CUDA_LDLIBS)
$(BUILDDIR)/$(LIB).so: $(BUILDDIR)/fsi_bandwidth_test.o $(BUILDDIR)/copy_to_gpu.o
	$(NVCC) -g $< -shared -o $@ $(CUDA_LDLIBS)

$(BUILDDIR)/fsi_bandwidth_test.o: fsi_bandwidth_test.c
	$(CC) $(CFLAGS) -fPIC -c $< -o $@
$(BUILDDIR)/copy_to_gpu.o: copy_to_gpu.cu
	$(NVCC) $(NVCCFLAGS) $< -o $@ $(GENCODE_FLAGS) 

	
clean:
	rm -f *.[oa] *.so $(BUILDDIR)/*.so  $(BUILDDIR)/*.[oa]
